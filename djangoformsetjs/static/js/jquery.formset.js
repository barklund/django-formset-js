/**
* Django formset helper
*/
(function($) {
    "use strict";

    var pluginName = 'formset';

    /**
    * Wraps up a formset, allowing adding, and removing forms
    */
    var Formset = function(el, options) {
        var _this = this;

        //Defaults:
        this.opts = $.extend({}, Formset.defaults, options);

        this.$formset = $(el);
        this.$emptyForm = this.$formset.find(this.opts.emptyForm);
        this.$body = this.$formset.find(this.opts.body);
        this.$add = this.$formset.find(this.opts.add);

        this.formsetPrefix = $(el).data('formset-prefix');
        
        if (this.opts.ordering) {
        	this.orderForms();
        }

        // Bind to the `Add form` button
        this.addForm = $.proxy(this, 'addForm');
        this.$add.click(this.addForm);

        // Bind receiver to `formAdded` and `formDeleted` events
        this.$formset.on('formAdded formDeleted', this.opts.form, $.proxy(this, 'checkMaxForms'));

        // Set up the existing forms
        this.$forms().each(function(i, form) {
            var $form = $(form);
            _this.bindForm($(this), i);
        });

        // Store a reference to this in the formset element
        this.$formset.data(pluginName, this);

        var extras = ['animateForms'];
        $.each(extras, function(i, extra) {
            if ((extra in _this.opts) && (_this.opts[extra])) {
                _this[extra]();
            }
        });
        
        this.disableOrders();
    };

    Formset.defaults = {
        form: '[data-formset-form]',
        emptyForm: 'script[type=form-template][data-formset-empty-form]',
        body: '[data-formset-body]',
        add: '[data-formset-add]',
        deleteButton: '[data-formset-delete-button]',
        upButton: '[data-formset-up-button]',
        downButton: '[data-formset-down-button]',
        hasMaxFormsClass: 'has-max-forms',
        animateForms: false,
        ordering: false
    };

    Formset.prototype.addForm = function() {
        // Don't proceed if the number of maximum forms has been reached
        if (this.hasMaxForms()) {
            throw new Error("MAX_NUM_FORMS reached");
        }

        var newIndex = this.totalFormCount();
        this.$managementForm('TOTAL_FORMS').val(newIndex + 1);

        var newFormHtml = this.$emptyForm.html()
            .replace(new RegExp('__prefix__', 'g'), newIndex)
            .replace(new RegExp('<\\\\/script>', 'g'), '</script>');

        var $newFormFragment = $($.parseHTML(newFormHtml, this.$body.document, true));
        this.$body.append($newFormFragment);

        var $newForm = $newFormFragment.filter(this.opts.form);
        this.bindForm($newForm, newIndex);

        this.disableOrders();
        
        return $newForm;
    };

    /**
    * Attach any events needed to a new form
    */
    Formset.prototype.bindForm = function($form, index) {
		var prefix = this.formsetPrefix + '-' + index;
    	
    	if (this.opts.ordering && $form.data("index") !== undefined) {
    		prefix = this.formsetPrefix + '-' + $form.data("index");
    	}
        
        var self = this;
    	
        $form.data(pluginName + '__formPrefix', prefix);

        var $delete = $form.find('[name=' + prefix + '-DELETE]');

        // Trigger `formAdded` / `formDeleted` events when delete checkbox value changes
        $delete.change(function(event) {
        		//console.log("is it checked",this,$delete.is(':checked'));
            if ($delete.is(':checked')) {
                $form.attr('data-formset-form-deleted', '');
                $form.trigger('formDeleted');
            } else {
                $form.removeAttr('data-formset-form-deleted');
                $form.trigger('formAdded');
            }
        }).trigger('change');

        var $deleteButton = $form.find(this.opts.deleteButton);

        $deleteButton.bind('click', function() {
            $delete.attr('checked', true).change();
            
            var postanim = function() {};
            
            if (self.opts.ordering) {
				postanim = function() {
					$form.nextAll(self.opts.form).not('[data-formset-form-deleted]').each(function() {
						var $next = $(this);
						
						self.swapElements($form, $next);
						
						var neworder = parseInt($next.attr("data-order")) - 1;
						$next.attr("data-order", neworder);
						$next.find('[name$="-ORDER"]').val(neworder);
					});
					
					var max = self.$forms().not('[data-formset-form-deleted]').size();
					
					$form.attr('data-order', max);
					$form.find('[name$="-ORDER"]').val(max);
					
					self.disableOrders();
				}
            }
            
            if (self.animateForms) {
            	$form.slideUp(postanim);
            } else {
            	postanim();
            }
        });
        
        if (this.opts.ordering) {
        	var $deleteds = this.$forms().filter('[data-formset-form-deleted]');
        	
        	var $order = $form.find('[name=' + prefix + '-ORDER]');
			if (!$order.val()) {
				$form.attr("data-order", index - $deleteds.size());
				$order.val(index - $deleteds.size());
				
				if (!$.fn.reverse) {
					$.fn.reverse = [].reverse;
				}
			
				$deleteds.reverse().each(function() {
					var $deleted = $(this);
					var newid = parseInt($deleted.attr("data-order")) + 1;
					$deleted.attr("data-order", newid);
					$deleted.find('[name$=-ORDER]').val(newid);
					self.swapElements($deleted, $form);
				});
			} else {
				$form.attr("data-order", $order.val());
			}
			
			
			var move = function(diff) {
				var oldorder = parseInt($order.val());
				var neworder = oldorder + diff;
				$order.val(neworder).change();
				var $otherForm = $(self.opts.form).filter('[data-order='+neworder+']')
				var $otherOrder = $otherForm.find('[name$=-ORDER]');
				$otherOrder.val(oldorder);
				
				if (!self.opts.animateForms) {
					// swap forms
					$form.attr("data-order", neworder);
					$otherForm.attr("data-order", oldorder);
					self.swapElements($form, $otherForm);
					self.disableOrders();
				} else {
					// fade out, swap, fade back in
					$form.fadeOut('fast');
					$otherForm.fadeOut('fast', function() {
						self.swapElements($form, $otherForm);
						$form.fadeIn('fast');
						$otherForm.fadeIn('fast', function() {
							$form.attr("data-order", neworder);
							$otherForm.attr("data-order", oldorder);
							self.disableOrders();
						});	
					});
				}
			}
			
			var $upButton = $form.find(this.opts.upButton);
			var $downButton = $form.find(this.opts.downButton);
	
			$upButton.bind('click', function() {
				move(-1);
			});
			$downButton.bind('click', function() {
				move(1);
			});
		}
    };

    Formset.prototype.$forms = function() {
        return this.$body.find(this.opts.form);
    };
    Formset.prototype.$managementForm = function(name) {
        return this.$formset.find('[name=' + this.formsetPrefix + '-' + name + ']');
    };

    Formset.prototype.totalFormCount = function() {
        return this.$forms().length;
    };

    Formset.prototype.deletedFormCount = function() {
        return this.$forms().filter('[data-formset-form-deleted]').length;
    };

    Formset.prototype.activeFormCount = function() {
        return this.totalFormCount() - this.deletedFormCount();
    };

    Formset.prototype.hasMaxForms = function() {
        var maxForms = parseInt(this.$managementForm('MAX_NUM_FORMS').val(), 10) || 1000;
        return this.activeFormCount() >= maxForms;
    };

    Formset.prototype.checkMaxForms = function() {
        if (this.hasMaxForms()) {
            this.$formset.addClass(this.opts.hasMaxFormsClass);
            this.$add.attr('disabled', 'disabled');
        } else {
            this.$formset.removeClass(this.opts.hasMaxFormsClass);
            this.$add.removeAttr('disabled');
        }
    };

    Formset.prototype.animateForms = function() {
    	var self = this;
        this.$formset.on('formAdded', this.opts.form, function() {
            var $form = $(this);
            $form.slideUp(0);
            $form.slideDown();
        });
        this.$forms().filter('[data-formset-form-deleted]').slideUp(0);
    };

    Formset.prototype.orderForms = function() {
    	var $forms = this.$forms();
    	$forms.each(function(i) {
    		$(this).attr("data-order", $(this).find('[name$="-ORDER"]').val());	
    		$(this).data("index", i);
    	});
    	for (var i = 0; i < $forms.length; i++) {
    		var $form = $forms.filter('[data-order='+i+']');
    		if ($form.get(0) != this.$forms().get(i)) {
    			this.swapElements($form, this.$forms().eq(i));
    		}
    	}
    };
    
    Formset.prototype.swapElements = function($el1, $el2) {
		var $tmp = $('<span>');
		$tmp.insertBefore($el2);
		$el2.insertBefore($el1);
		$el1.insertBefore($tmp);
		$tmp.remove();
	}

    Formset.prototype.disableOrders = function() {
    	var $forms = this.$forms();
    	$forms.find(this.opts.upButton).removeAttr("disabled");
    	$forms.filter('[data-order=0]').find(this.opts.upButton).attr("disabled","disabled");
    	
    	var max = $forms.not('[data-formset-form-deleted]').size() - 1;
    	
    	$forms.find(this.opts.downButton).removeAttr("disabled");
    	$forms.filter('[data-order='+max+']').find(this.opts.downButton).attr("disabled","disabled");
    };

    Formset.getOrCreate = function(el, options) {
        var rev = $(el).data(pluginName);
        if (!rev) {
            rev = new Formset(el, options);
        }

        return rev;
    };

    $.fn[pluginName] = function() {
        var options, fn, args;
        // Create a new Formset for each element
        if (arguments.length === 0 || (arguments.length === 1 && $.type(arguments[0]) != 'string')) {
            options = arguments[0];
            return this.each(function() {
                return Formset.getOrCreate(this, options);
            });
        }

        // Call a function on each Formset in the selector
        fn = arguments[0];
        args = $.makeArray(arguments).slice(1);

        if (fn in Formset) {
            // Call the Formset class method if it exists
            args.unshift(this);
            return Formset[fn].apply(Formset, args);
        } else {
            throw new Error("Unknown function call " + fn + " for $.fn.formset");
        }
    };
})(jQuery);
